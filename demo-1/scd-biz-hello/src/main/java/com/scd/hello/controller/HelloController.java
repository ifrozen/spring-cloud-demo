package com.scd.hello.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/echo")
	public String echo(String name) {
		return "hello  " + name;
	}
	
}
