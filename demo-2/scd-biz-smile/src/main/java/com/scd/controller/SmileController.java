package com.scd.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SmileController {

	@Value("${smile}")
	private String smile;
	
	@GetMapping("/config")
	public String config() {
		return smile;
	}
	
}
