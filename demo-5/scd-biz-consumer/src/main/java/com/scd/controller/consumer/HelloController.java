package com.scd.controller.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scd.feign.client.ProviderFeignClient;

@RestController
public class HelloController {
	@Autowired
	private ProviderFeignClient providerFeignClient;
	
	@GetMapping("/sayHello")
	public String sayHello(String name) {
		return providerFeignClient.sayHello(name);
	}
	
	
}
