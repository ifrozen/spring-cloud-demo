package com.scd.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//name为服务提供者的spring.application.name属性值
@FeignClient(name = "scd-base-provider")
public interface ProviderFeignClient {

	//要加上服务提供者的server.servlet.context-path属性值为前缀
	//参数的话，必须加上@RequestParam注解
	@GetMapping("/provider/sayHello")
	public String sayHello(@RequestParam("name") String name);
	
}
