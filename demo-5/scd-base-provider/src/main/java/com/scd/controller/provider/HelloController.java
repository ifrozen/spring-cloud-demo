package com.scd.controller.provider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/sayHello")
	public String sayHello(String name) {
		return "Hello " + name + "!";
	}
	
}
